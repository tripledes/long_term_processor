package org.mdas.ltprocessor.infrastructure.config;

import java.io.IOException;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.jboss.logging.Logger;

@ApplicationScoped
public class ElasticsearchRestClient {

    private static final Logger logger = Logger.getLogger(ElasticsearchRestClient.class);

    private final ElasticsearchConfig configuration;

    private RestHighLevelClient restHighLevelClient;

    public ElasticsearchRestClient(ElasticsearchConfig configuration) {
        this.configuration = configuration;
    }

    public boolean connect() {
        try {
            initRestHighLevelClient();
            createIndex();
        } catch (Exception e){
            logger.errorv(e, "Exception when trying to connect to Elasticsearch");
            return false;
        }
        return true;
    }

    private void initRestHighLevelClient() {
        SSLContext sc = HttpConnectionManager.sslContext();
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials(configuration.username, configuration.password));

        final RestClientBuilder builder = RestClient
                .builder(new HttpHost(configuration.hostname, configuration.port, configuration.scheme))
                .setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder.setSSLContext(sc)
                        .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                        .setDefaultCredentialsProvider(credentialsProvider));

        restHighLevelClient = new RestHighLevelClient(builder);
    }

    public void send(String id, Map<String, Object> document) {
        IndexRequest request = new IndexRequest(configuration.index)
                .id(id)
                .source("temperature", document.get("temperature"), 
                        "sensorId", document.get("sensorId"), 
                        "area", document.get("area"), 
                        "location", document.get("location"), 
                        "timestamp", document.get("timestamp"))
                .opType(DocWriteRequest.OpType.INDEX);
        try {
            restHighLevelClient.index(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            logger.errorv(e, "Could not send message {0} with id {1}", document.toString(), id);
        }
    }

    private void createIndex() throws IOException {
        try {
            if (!indexExists()) {
                CreateIndexResponse createIndexResponse = restHighLevelClient.indices().create(createIndexRequest(),
                        RequestOptions.DEFAULT);
                if (!createIndexResponse.isAcknowledged()) {
                    logger.errorv("could not create index {0}", configuration.index);
                }
            }
        } catch (IOException e) {
            logger.errorv(e, "Error when trying to connect to index {0}", configuration.index);
            throw e;
        }
    }

    private CreateIndexRequest createIndexRequest() throws IOException {
        CreateIndexRequest request = new CreateIndexRequest(configuration.index)
            .settings(Settings.builder()
                .put("index.number_of_shards", configuration.instances)
                .put("index.number_of_replicas", configuration.instances));

        XContentBuilder mapping = XContentFactory.jsonBuilder();
        mapping.startObject();
        {
            mapping.startObject("properties")
                        .startObject("temperature").field("type", "float").endObject()
                        .startObject("sensorId").field("type", "text").endObject()
                        .startObject("area")
                            .field("type", "text")
                            .field("fielddata", true)
                        .endObject()
                        .startObject("location").field("type", "geo_point").endObject()
                        .startObject("timestamp").field("type", "long").endObject()
                    .endObject();
        }
        mapping.endObject();
        request.mapping(mapping);
        return request;
    }

    private boolean indexExists() throws IOException {
        return restHighLevelClient.indices().exists(new GetIndexRequest(configuration.index), RequestOptions.DEFAULT);
    }

    public void close() {
        try {
            logger.infov("Closing elasticsearch connector");
            restHighLevelClient.close();
        } catch (IOException e) {
            logger.error("Error on trying to close elasticsearch connector", e);
        }
    }

    public String indexName() {
        return configuration.index;
    }
}