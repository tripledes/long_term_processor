package org.mdas.ltprocessor.infrastructure.config;

import io.quarkus.arc.config.ConfigProperties;

@ConfigProperties
public class ElasticsearchConfig {
    public String index;

    public String hostname;

    public int port;

    public String scheme;

    public int instances;

    public String username;

    public String password;
}
