package org.mdas.ltprocessor.infrastructure;

import io.quarkus.kafka.client.serialization.JsonbDeserializer;
import org.mdas.ltprocessor.model.Measure;

public class MeasureDeserializer extends JsonbDeserializer<Measure> {
    public MeasureDeserializer() {
        super(Measure.class);
    }
}