package org.mdas.ltprocessor.model;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;

public class Sensor {
    private final String id;
    private final String area;
    private final Location location;

    @JsonbCreator
    public Sensor(@JsonbProperty("id") String id,
                    @JsonbProperty("area") String area,
                    @JsonbProperty("location") Location location) {
        this.id = id;
        this.area = area;
        this.location = location;
    }

    public String getId() {
        return id;
    }

    public String getArea() {
        return area;
    }

    public Location getLocation() {
        return location;
    }

    @Override
    public String toString() {
        return "{" +
                "id='" + id + '\'' +
                ", area='" + area + '\'' +
                ", location=" + location +
                '}';
    }
}
