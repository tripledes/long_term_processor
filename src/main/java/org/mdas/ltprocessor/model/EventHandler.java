package org.mdas.ltprocessor.model;

public interface EventHandler<T> {
    void handle(T event);
    boolean isConnected();
}
