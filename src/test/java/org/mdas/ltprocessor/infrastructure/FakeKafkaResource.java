package org.mdas.ltprocessor.infrastructure;

import java.util.Map;

import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import io.smallrye.reactive.messaging.connectors.InMemoryConnector;

/**
 * Use the in-memory connector to avoid having to use a broker.
 */
public class FakeKafkaResource implements QuarkusTestResourceLifecycleManager {

    @Override
    public Map<String, String> start() {
        return InMemoryConnector.switchIncomingChannelsToInMemory("measure-topic");
    }

    @Override
    public void stop() {
        InMemoryConnector.clear();
    }
}
