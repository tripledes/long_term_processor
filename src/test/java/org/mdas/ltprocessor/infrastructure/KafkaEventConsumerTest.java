package org.mdas.ltprocessor.infrastructure;

import static org.mockito.Mockito.verify;

import javax.enterprise.inject.Any;
import javax.inject.Inject;

import org.junit.jupiter.api.Test;
import org.mdas.ltprocessor.model.Measure;
import org.mdas.ltprocessor.mothers.MeasureObjectMother;
import org.mockito.Mockito;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusMock;
import io.quarkus.test.junit.QuarkusTest;
import io.smallrye.reactive.messaging.connectors.InMemoryConnector;

@QuarkusTest
@QuarkusTestResource(FakeKafkaResource.class)
public class KafkaEventConsumerTest {

    @Inject
    @Any
    InMemoryConnector connector;
    
    @Test
    public void shouldCallHandlerWhenConsumingFromMeasureTopic(){        
        ElasticsearchEventHandler mockHandler = Mockito.mock(ElasticsearchEventHandler.class);
        QuarkusMock.installMockForType(mockHandler, ElasticsearchEventHandler.class);  

        Measure measure = MeasureObjectMother.measureFromMontcada();
        
        connector.source("measure-topic").send(measure);

        verify(mockHandler).handle(measure);
    }
}